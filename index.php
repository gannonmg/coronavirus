<!doctype html>

<html>
<head>
    <!--please be sure to switch :) to :( if the status of this webpage changes.-->
    <title>:)</title>
</head>
<body>
    <h1>No</h1>
    <p><?php 
        $tz = 'America/Chicago';
        $timestamp = filemtime("./index.php");
        $dt = new DateTime("now", new DateTimeZone($tz));
        $dt->setTimestamp($timestamp);
        echo "Updated: ".$dt->format('F d, Y h:i:s A')." CT";    
    ?></p>
</body>
</html>
